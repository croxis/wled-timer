import concurrent.futures
import datetime
import json
import math
import requests
import time

from tkinter import *
from tkinter import colorchooser
from tkinter import ttk
from tkinter.font import Font

from tkeys.keyboards import NumKeyPad

thread_pool_executor = concurrent.futures.ThreadPoolExecutor(max_workers=2)


class LightTimer(ttk.Frame):
    host = "http://" + "192.168.1.217"

    endtime = datetime.datetime.now()
    previous_state = {}
    timer_running = False
    progress_percent = 0
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        style = ttk.Style()
        style.theme_use('default')
        style.configure('My.TSpinbox', arrowsize=30)
        self.grid(column=0, row=0, sticky=(N, W, E, S))
        
        self.countdown_label = ttk.Label(self, text="0:0:0", font=Font(family='Helvetica', size=36, weight='bold'))
        self.countdown_label.grid(columnspan=3, row=0, sticky=(N, W, E, S))
        self.progress = ttk.Progressbar(self, orient=HORIZONTAL, mode='determinate', variable=self.progress_percent)
        self.progress.grid(columnspan=3, row=1, sticky=(W, E))
        self.progress_label = ttk.Label(self, text="0%")
        self.progress_label.grid(column=1, row=1)
        ttk.Label(self, text="H").grid(column=0, row=2)
        ttk.Label(self, text="M").grid(column=1, row=2)
        ttk.Label(self, text="S").grid(column=2, row=2)
        #self.h = ttk.Spinbox(self, style='My.TSpinbox', from_=0, font=Font(family='Helvetica', size=24, weight='bold'))
        self.h = ttk.Spinbox(self, style='My.TSpinbox', from_=0, width=2)
        self.m = ttk.Spinbox(self, style='My.TSpinbox', from_=0, to=59, width=2)
        self.s = ttk.Spinbox(self, style='My.TSpinbox', from_=0, to=59, width=2)
        self.h.grid(column=0, row=3, sticky=(N, W, E, S))
        self.m.grid(column=1, row=3, sticky=(N, W, E, S))
        self.s.grid(column=2, row=3, sticky=(N, W, E, S))
        self.h.set(0)
        self.m.set(0)
        self.s.set(0)
        
        self.start_button = ttk.Button(self, text="Start", command=self.on_start)
        self.start_button.grid(columnspan=2, row=4, sticky=(W, E))
        self.stop_button = ttk.Button(self, text="End", command=self.stop_timer)
        self.stop_button.grid(columnspan=2, row=5, sticky=(W, E))
        self.stop_button["state"] = "disabled"
        
        self.startcolor = (255, 255, 255)
        self.endcolor = (0, 255, 0)
        
        self.start_color_button = Button(self, bg="#ffffff", command=self.pick_start_color)
        self.start_color_button.grid(column=2, row=4, sticky=(W, E))
        self.end_color_button = Button(self, bg="#00ff00", command=self.pick_end_color)
        self.end_color_button.grid(column=2, row=5, sticky=(W, E))
        
        NumKeyPad(parent=self)
  
  
    def pick_start_color(self):
        color_code = colorchooser.askcolor(title ="Choose start color")
        self.startcolor = color_code[0]
        self.start_color_button.config(bg = color_code[1])
        print(color_code)


    def pick_end_color(self):
        color_code = colorchooser.askcolor(title ="Choose end color")
        self.endcolor = color_code[0]
        self.end_color_button.config(bg = color_code[1])
        print(color_code)


    def on_start(self):
        thread_pool_executor.submit(self.run_timer)
        

    def run_timer(self):
        self.deltacolor = [self.endcolor[0] - self.startcolor[0],
                    self.endcolor[1] - self.startcolor[1],
                    self.endcolor[2] - self.startcolor[2]]

        self.leds = json.loads(requests.get(self.host + "/json/info").text)['leds']['count']
        print(self.h.get(), type(self.h.get()))
        self.timespan = datetime.timedelta(hours=int(self.h.get()), minutes=int(self.m.get()), seconds=int(self.s.get()))
        self.starttime = datetime.datetime.now()
        self.endtime = datetime.datetime.now() + self.timespan

        self.timeperled = self.timespan / self.leds
        if not self.timer_running:
            self.start_button["state"] = "disabled"
            self.stop_button["state"] = "normal"
            self.previous_state = json.loads(requests.get(self.host + "/json/state").text)
            self.timer_running = True
            while datetime.datetime.now() <= self.endtime and self.timer_running:
                self.step()
            all_red = {"seg":[{"col":[[255,0,0]]}]}
            requests.post(self.host + '/json/state', data=json.dumps(all_red))
            
            time.sleep(1)
            requests.post(self.host + '/json/state', data=json.dumps({"on": False}))
            
            time.sleep(1)
            requests.post(self.host + '/json/state', data=json.dumps({"on": True}))
            
            time.sleep(1)
            requests.post(self.host + '/json/state', data=json.dumps({"on": False}))
            
            time.sleep(1)
            requests.post(self.host + '/json/state', data=json.dumps({"on": True}))
            time.sleep(1)
            requests.post(self.host + '/json/state', data=json.dumps({"on": False}))
            
            time.sleep(1)
            requests.post(self.host + '/json/state', data=json.dumps({"on": True}))
            time.sleep(1)
            requests.post(self.host + '/json/state', data=json.dumps(self.previous_state))
            self.timer_running = False
        self.start_button["state"] = "normal"
        self.stop_button["state"] = "disabled"
        self.countdown_label.config(text = "00:00:00")


    def step(self):
        timesincestart = datetime.datetime.now() - self.starttime
        percentcomplete = timesincestart/self.timespan
        cursor = math.floor(self.leds*percentcomplete)
        completeledtime = cursor * self.timeperled + self.starttime
        fractiontime = datetime.datetime.now() - completeledtime
        percentfractiontime = fractiontime/self.timeperled
        cursorcolor = [int(self.startcolor[0] + self.deltacolor[0]*percentfractiontime),
                    int(self.startcolor[1] + self.deltacolor[1]*percentfractiontime),
                    int(self.startcolor[2] + self.deltacolor[2]*percentfractiontime)]
        if cursor > 0:
            payload = {'seg': {'i': [0, cursor-1, self.endcolor, cursor, cursorcolor, cursor+1, self.leds, self.startcolor]}}
        else:
            payload = {'seg': {'i': [0, cursorcolor, 1, self.leds, self.startcolor]}}
        z = requests.post(self.host + '/json/state', data=json.dumps(payload))
        
        progress_percent = percentcomplete * 100
        remaining_time = self.endtime - datetime.datetime.now()
        hours, remainder = divmod(remaining_time.seconds, 3600)
        minutes, seconds = divmod(remainder, 60)
        self.progress['value'] = progress_percent
        new_text = str(round(progress_percent, 0)) + "%"
        self.progress_label.config(text = new_text)
        self.countdown_label.config(text = f"{hours}:{minutes}:{seconds}")
        print(round(percentcomplete, 2), cursor, self.leds, z.text, payload)

    def stop_timer(self):
        self.timer_running = False


if __name__ == "__main__":
    root = Tk()
    root.columnconfigure(0, weight=1)
    root.rowconfigure(0, weight=1)
    mainframe = LightTimer(root, padding="3 3 12 12")
    for i in range(3):
        mainframe.columnconfigure(i, weight=1)
    for i in range(5):
        mainframe.rowconfigure(i, weight=1)
    root.title("Radnet countdown")
    
    root.mainloop()
